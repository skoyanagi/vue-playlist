var playlist = Vue.component('playlist', {
	template: `
		<div>
			<div>	            
                <div class="form-group"> 
                    <label for="song">Song:</label>
                    <input type="text" class="form-control" v-model="title" v-on:keyup.enter="onEnterClick">
                </div>
                <div class="form-group">
                    <label for="artist">Artist:</label>
                    <input type="text" class="form-control" v-model="artist" v-on:keyup.enter="onEnterClick">
                </div>
                
                <button type="submit" class="btn btn-primary" @click="addNewSong(title, artist)">Add Song</button>
	            
        	</div>
        	<div class="jumbotron"> 
        		<h4 class="text-center">Playlist</h4>
        		<div v-for="(song, index) in songs"class="card">
            		<ul class="list-group list-group-flush">
                		<li class="list-group-item">Title: {{ song.title }}</li>
                		<li class="list-group-item">Artist: {{ song.artist }}</li>
            		</ul>
            		<button class="btn-danger" @click="removeItem(index)">Remove Song</button>
        		</div>
    		</div>
        </div>
	`,
 
	data() {
		
		return {
			title: "",
			artist: "",
			songs: [
				{
					title: 'Forever On Your Side',
					artist: 'Needtobreath'
				},
				{
					title: 'Sweet Caroline',
					artist: 'Neil Diamond'
				},
				{
					title: 'Bohemian Rhapsody',
					artist: 'Queen'
				}
			]
		}
	},
	methods: {
		addNewSong(title, artist) {
			var song = new Object();
			song.title = title;
			song.artist = artist;
			this.songs.push(song);
			this.title = "";
			this.artist = "";
		},

		onEnterClick: function() {
    		this.addNewSong(this.title, this.artist);
    		this.title = "";
			this.artist = "";
    	},
    	removeItem(index) {
    		this.songs.splice(index, 1);
    	}

	}
}) 

var app = new Vue({
	el: "#app"
})